cmake_minimum_required(VERSION 3.22)
project(neuronal1)

set(CMAKE_C_STANDARD 11)

add_executable(neuronal1 main.c src/exo1.c src/exo1.h)
target_link_libraries(neuronal1 m)