//
// Created by alexis on 24/05/22.
//

#ifndef NEURONAL1_EXO1_H
#define NEURONAL1_EXO1_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>

void exo1();
void exo1_1();

#endif //NEURONAL1_EXO1_H
