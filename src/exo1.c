//
// Created by alexis on 24/05/22.
//

#include "exo1.h"

#define PATTERN_SIZE 48
#define NUMBER_PATTERN 10

FILE *train_file;
FILE *applied_file;
int stats_i = 0;

float epsilon = 0.01f;
float teta = 0.5f;

typedef struct pattern {
    int x[PATTERN_SIZE + 1];
    int k;
} Pattern;

typedef struct network {
    Pattern *patterns[NUMBER_PATTERN];
    float w1[NUMBER_PATTERN][PATTERN_SIZE + 1];
} Network;

Pattern *read_pattern(char *file_name) {
    FILE *file = fopen(file_name, "r");

    if (file == NULL) {
        printf("cannot read file !\n");
        return NULL;
    }

    Pattern *pPattern = malloc(sizeof *pPattern);

    int count = 0;
    char c;
    while ((c = (char) getc(file)) != EOF) {
        if (c == '.') {
            pPattern->x[count] = 0;
            count++;
        } else if (c == '*') {
            pPattern->x[count] = 1;
            count++;
        } else if (c == '0') {
            pPattern->k = 0;
        } else if (c == '1') {
            pPattern->k = 1;
        }
    }
    pPattern->x[PATTERN_SIZE] = 1;

    fclose(file);
    return pPattern;
}

float calc_pot_i(Network *network, Pattern *pattern, int xi) {
    float pot_i = 0.0f;
    for (int i = 0; i < PATTERN_SIZE; ++i)
        pot_i += (float) pattern->x[i] * network->w1[xi][i];

    pot_i += 1.0f * network->w1[xi][PATTERN_SIZE];

    return pot_i;
}

int calc_x(float pot_i) {
    return pot_i > teta;
}

bool test_network(Network *network) {
    float error = 0.0f;
    printf("Test Pattern -> \n");
    for (int i = 0; i < NUMBER_PATTERN; ++i) {
        Pattern *pattern = network->patterns[i];

        for (int j = 0; j < NUMBER_PATTERN; ++j) {

            float pot_i = calc_pot_i(network, pattern, j);
            float x = (float)calc_x(pot_i);
            x = pot_i;

            if (j == i) {
                x -= (float)pattern->k;
            }

            error += fabsf(x);

        }

//        printf("\t %d -> Pot_i : %f, X : %f, delta : %f, Error : %f\n", pattern->k, pot_i, x, delta, fabsf(delta));
    }
    stats_i++;
    fprintf(train_file, "%d %f\n", stats_i, error);
    printf("Total Error : %f\n\n", error);

    return error < 0.01;
}

void training(Network *network) {
    srand(time(NULL));

    while (!test_network(network)) {

        int pattern_choice = rand() % NUMBER_PATTERN;

        /** select random patterns */
        Pattern *pPattern = network->patterns[pattern_choice];
        printf("Training Pattern : %d -> \n", pPattern->k);

        for (int i = 0; i < NUMBER_PATTERN; ++i) {
            float pot_i = calc_pot_i(network, pPattern, i);
            float delta = 0 - pot_i;

            if (i == pattern_choice) {
                delta = 1 - pot_i;
            }

            for (int j = 0; j < PATTERN_SIZE; ++j) {
                /* Wij(t + 1) = Wij(t) + ε(Yd-Yi) * Xj */
                network->w1[i][i] = network->w1[i][j] + epsilon * delta * (float) pPattern->x[j];
            }
            network->w1[i][PATTERN_SIZE] = network->w1[i][PATTERN_SIZE] + epsilon * delta * (float) pPattern->x[PATTERN_SIZE];

            printf("\tX%d, pot_i : %f, delta : %f\n", i, pot_i, delta);
        }
    }
}

void init_w(Network *network) {
    for (int i = 0; i < NUMBER_PATTERN; ++i) {
        for (int j = 0; j < PATTERN_SIZE; ++j) {
            network->w1[i][j] = (float) rand() / (float) RAND_MAX / PATTERN_SIZE;
        }
        //    network->w1[PATTERN_SIZE] = -teta;
        network->w1[i][PATTERN_SIZE] = 0;
    }
}

int applied(Network *network, Pattern *pattern) {
    int x = calc_x(calc_pot_i(network, pattern, 0));
    return x == pattern->k;
}

void pattern_copy(Pattern *pattern, Pattern *newPattern) {

    for (int i = 0; i < PATTERN_SIZE; i++) {
        newPattern->x[i] = pattern->x[i];
    }

    newPattern->k = pattern->k;
}

bool int_contains(const int array[], int size, int value) {
    for (int i = 0; i < size; i++)
        if (array[i] == value)
            return true;

    return false;
}

void noise(Pattern *pattern, int rate) {
    if (rate > 100 || rate <= 0)
        return;

    int a = (int)ceilf((float)rate / 100 * 48);
    int value[a];

    /**
     * generate {a} unique rand value
     */
    for (int i = 1; i < a; i++) {
        int r = rand() % 48;

        while (int_contains(value, a, r)) r = rand() % 48;

        value[i] = r;
        pattern->x[r] = !pattern->x[r];
    }
}

void exo1_1() {
    train_file = fopen("train.txt", "w+");
    applied_file = fopen("applied.txt", "w+");

    Network *network = malloc(sizeof(Network));
    network->patterns[0] = read_pattern("../files/0.txt");
    network->patterns[1] = read_pattern("../files/1.txt");
    network->patterns[2] = read_pattern("../files/2.txt");
    network->patterns[3] = read_pattern("../files/3.txt");
    network->patterns[4] = read_pattern("../files/4.txt");
    network->patterns[5] = read_pattern("../files/5.txt");
    network->patterns[6] = read_pattern("../files/6.txt");
    network->patterns[7] = read_pattern("../files/7.txt");
    network->patterns[8] = read_pattern("../files/8.txt");
    network->patterns[9] = read_pattern("../files/9.txt");
    init_w(network);
    training(network);

    Pattern *copy = malloc(sizeof(Pattern));

    for (int i = 0; i < 98; i += 2) {

        fprintf(applied_file, "%d ", i);

        for (int j = 0; j < NUMBER_PATTERN; j++) {
            int a = 0;
            for (int k = 0; k < 50; k++) {
                pattern_copy(network->patterns[j], copy);
                noise(copy, i);

                a += abs(applied(network, copy) - 1);
            }

            fprintf(applied_file, "%d ", a);
        }

        fprintf(applied_file, "\n");
        fprintf(applied_file, "%d\n", i+1);
    }

}